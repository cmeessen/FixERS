#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import absolute_import, division, print_function
import copy
import sys
import os
import argparse
import traceback
import tempfile
import shutil


# script version
version = "FixERS.py v0.3"

# File extensions of files to process in recursive processing
fileTypes = ['.cxx', '.cpp', '.h', '.hpp', '.hxx', '.i', '.C', '.cc']

# Subdirectories to ignore in recursive processing
skipDirs = ['.svn', '.git', '.hg']

macros = ["FATAL", "ERROR", "WARNING", "INFO", "LOG", "DEBUG",
          "ASSERT_MSG", "ASSERT"]

def getFileAsString(filename):
    """Read file as string"""
    with open(filename) as f:
        return f.read()


class ErsMacroStream(object):
    """Turn a string into a char stream"""
    __str = ""
    __idx = 0
    __len = 0
    __lineIdx = [0]
    __lineNo = 0
    valid = 0
    missing = 0
    invalid = 0
    __grey = ("<grey>", "</grey>")
    __white = ("<white>", "</white>")
    __green = ("<green>", "</green>")
    __red = ("<red>", "</red>")
    __match = []

    def __init__(self, string):
        """Construct ErsMacroStream with the given string"""
        self.__str = string
        self.__idx = 0
        self.__len = len(string)
        self.__lineIdx = [0]
        self.__match = []
        nm = self.__nextMacro()
        while nm is not None:
            self.__match.append(nm)
            nm = self.__nextMacro()

    def __char(self):
        """Return the next char or '\0' if at end of stream"""
        return self.__str[self.__idx] if self.__idx < self.__len else None

    def __peekNext(self):
        """Return the next char without incrementing the index"""
        return self.__str[self.__idx + 1] if self.__idx < self.__len else None

    def __nextChar(self):
        """Move to and return next char, or return '\0' if at end of string"""
        c = self.__char()
        #k = c if c >= ' ' else "«{}»".format(ord(c))
        #print(self.__idx, k, self.__lineNo, self.__lineIdx)
        if c is None:
            if len(self.__lineIdx) is 0 or self.__lineIdx[-1] != self.__len:
                self.__lineIdx.append(self.__len)
            return c
        self.__idx += 1
        if c == '\n':
            self.__lineNo += 1
            self.__lineIdx.append(self.__idx)
        if self.__idx == self.__len:
            self.__lineIdx.append(self.__idx)
        return self.__char()

    def __filteredChar(self):
        """current char skipping comments, quoted strings and \r"""
        c = self.__char()
        while c is not None:
            if c == '\r':
                # skip carriage return
                c = self.__nextChar()
            elif c == '"':
                # Skip strings
                c = self.__nextChar()
                while c != '"':
                    if c is None:
                        return c
                    if c == '\\':
                        c = self.__nextChar()
                    c = self.__nextChar()
                c = self.__nextChar()
            elif c == "'":
                # Skip strings
                c = self.__nextChar()
                while c != "'":
                    if c is None:
                        return c
                    if c == '\\':
                        c = self.__nextChar()
                    c = self.__nextChar()
                c = self.__nextChar()
            elif c == '/':
                if self.__peekNext() == '*':
                    # skip C comment /* .. */
                    self.__nextChar()
                    c = self.__nextChar()
                    while not (c == '*' and self.__peekNext() == '/'):
                        if c is None:
                            return c
                        c = self.__nextChar()
                    self.__nextChar()
                    c = self.__nextChar()
                elif self.__peekNext() == '/':
                    # skip C99 or Cpp comment // .. \n
                    self.__nextChar()
                    c = self.__nextChar()
                    while c != '\n':
                        if c is None:
                            return c
                        c = self.__nextChar()
                else:
                    break
            else:
                break
        return c

    def __nextFilteredChar(self):
        self.__nextChar()
        return self.__filteredChar()

    def __skipMacroArguments(self):
        """Skip macro arguments up to the first char after ')'
        return  0 if found ')' after arguments,
        return -1 if didn't found '(' after macro name
        return -2 if end of string is reached before ')' was found"""
        c = self.__filteredChar()
        while c == ' ' or c == '\t' or c == '\n':
            c = self.__nextFilteredChar()
        if c != '(':
            return -1
        paren = 1
        c = self.__nextFilteredChar()
        while c is not None:
            if c == '(':
                paren += 1
            elif c == ')':
                paren -= 1
                if paren is 0:
                    self.__nextChar()
                    return 0
            c = self.__nextFilteredChar()
        return -2

    def __nextMacro(self):
        """Return tupple for next macro info found or None if end of
           string is reached. Tuple content:
           [0] status:
                1 if ; is found after ) of macro arguments,
                0 if ; not found after ) of macro arguments,
               -1 if ( not found after macro name,
               -2 if end of string reached before ) was found
           [1] is one of "FATAL", "ERROR", "WARNING", "INFO", "LOG", "DEBUG"
           [2] is index of first char of macro name ERS_...
           [3] is index of ; after ) of macro if status is 1
               is index of insertion point of ; after ) if status is 0
               is first none space char after macro name if status is -1
               is string length if status is -2
           [4] lineNo containing index of first char [2]
           [5] lineNo containing index of last char [3]
           index are numbered from 0 to n-1
           """
        c = self.__filteredChar()
        while c is not None:
            if (c == 'E' and
                self.__idx + 7 < self.__len and
                self.__str[self.__idx + 1] == 'R' and
                self.__str[self.__idx + 2] == 'S' and
                self.__str[self.__idx + 3] == '_'):
                for macro in macros:
                    tmpStartIndex = self.__idx + 4
                    tmpEndIndex = tmpStartIndex + len(macro)
                    if (tmpEndIndex <= self.__len and
                            self.__str[tmpStartIndex:tmpEndIndex] == macro):
                        startLineNo = self.__lineNo
                        startIndex = self.__idx
                        self.__idx = tmpEndIndex  # skip macro name
                        status = self.__skipMacroArguments()
                        endIndex = self.__idx
                        endLineNo = self.__lineNo
                        if status is 0:
                            c = self.__filteredChar()
                            while c == ' ' or c == '\t' or c == '\n':
                                c = self.__nextFilteredChar()
                            if c == ';':
                                status = 1
                                endIndex = self.__idx
                                endLineNo = self.__lineNo
                        elif status > 1 or status < -2:
                            raise Exception("Invalid status value")
                        if status is 1:
                            self.valid += 1
                        elif status is 0:
                            self.missing += 1
                        else:
                            self.invalid += 1
                        return (status, macro, startIndex, endIndex,
                                startLineNo, endLineNo)
            self.__nextChar()
            c = self.__filteredChar()
        return None

    def getMacroTuples(self):
        """Return a deep copy of the macro tuples"""
        return copy.deepcopy(self.__match)

    def setColors(self, colors={
                    "grey": ("<grey>", "</grey>"),
                    "white": ("<white>", "</white>"),
                    "green": ("<green>", "</green>"),
                    "red": ("<red>", "</red>")}):
        """Set the colors used by getMacroLine"""
        if colors is None:
            colors = {"grey": ("", ""), "white": ("", ""),
                      "green": ("", ""), "red": ("", "")}
        self.__grey = colors["grey"]
        self.__white = colors["white"]
        self.__green = colors["green"]
        self.__red = colors["red"]

    def setBashColors(self):
        """Set colors for Bash shell"""
        self.setColors({
                    "grey": ("\033[1;30m", "\033[0m"),
                    "white": ("", ""),
                    "green": ("\033[0;32m", "\033[0m"),
                    "red": ("\033[0;31m", "\033[0m")})

    def substring(self, beginIdx, endIdx):
        """Return a substring in the range fromIdx to toIdx"""
        return self.__str[beginIdx:endIdx]

    def statusStr(self):
        """Return a status string"""
        if len(self.__match) is 0:
            return "0 macros"
        res = ''
        if self.valid is not 0:
            res += self.__green[0] + str(self.valid) + self.__green[1] + \
                    " with ; "
        if self.missing is not 0:
            res += self.__red[0] + str(self.missing) + self.__red[1] + \
                    " without ; "
        if self.invalid is not 0:
            res += self.__red[0] + str(self.invalid) + self.__red[1] + \
                    " invalid"
        return res

    def asString(self, macroTuple, nbrLines=0):
        """This method generates a string displaying a macro match result.

           Its intended use is to provide a feedback to the user.
           The returned string is the macro with the line number in front
           where the macro was found when nbrLines is 0.
           If color is not None, it displays the macro instruction in white,
           the semi column in green if present, or in red if absent and
           where it will be inserted.
           If nbrLines is not 0, the return string will contain full lines
           with nbrLines before and nbrLines after. If color is not None
           anything not part of the macro and it's semi column is in grey.
           """
        mt = macroTuple
        if nbrLines < 0:
            nbrLines = 0
        macroColor = self.__white if mt[0] >= 0 else self.__red

        # output lineNo and grey string in front of macro
        res = self.__white[1] + self.__grey[0]
        lineNo = mt[4] - nbrLines
        if lineNo < 0:
            lineNo = 0
        startIdx = self.__lineIdx[lineNo]
        while lineNo < mt[4]:
            endIdx = self.__lineIdx[lineNo + 1]
            res += "{0:04}: {1}".format(lineNo, self.__str[startIdx:endIdx])
            startIdx = endIdx
            lineNo += 1
        endIdx = mt[2]
        res += "{0:04}: {1}".format(lineNo, self.__str[startIdx:endIdx])
        startIdx = endIdx
        res += self.__grey[1] + macroColor[0]

        # if macro is spread over multiple lines
        while lineNo != mt[5]:
            lineNo += 1
            endIdx = self.__lineIdx[lineNo]
            res += self.__str[startIdx:endIdx]
            startIdx = endIdx
            res += macroColor[1] + self.__grey[0]
            res += "{0:04}: ".format(lineNo)
            res += self.__grey[1] + macroColor[0]

        # output remain of macro and line in grey if any
        endIdx = mt[3]
        res += self.__str[startIdx:endIdx] + macroColor[1]
        startIdx = endIdx
        if mt[0] == 1:
            res += self.__green[0] + ';' + self.__green[1]
            startIdx += 1
        elif mt[0] == 0:
            res += self.__red[0] + ';' + self.__red[1]
        endIdx = self.__lineIdx[lineNo + 1]
        if nbrLines == 0 and startIdx == endIdx:
            return res + self.__white[0]
        res += self.__grey[0]
        res += self.__str[startIdx:endIdx]
        lineNo += 1
        endLine = mt[5] + nbrLines + 1
        if endLine >= len(self.__lineIdx):
            endLine = len(self.__lineIdx) - 1
        while lineNo < endLine:
            startIdx = endIdx
            endIdx = self.__lineIdx[lineNo + 1]
            res += "{:04}: {}".format(lineNo, self.__str[startIdx:endIdx])
            lineNo += 1
        res += self.__grey[1] + self.__white[0]
        return res

    @staticmethod
    def unitTest():
        """Make sure the class behaves as expected"""

        # Test string and comment filtering
        test1In = """
0
/* c */1
/** c */2
/* c /* c */3
4// e
5// c /* c */
/* /**//**/6
"s"7
8"sss\\"s"
"sss\\\"s"9
"""
        test1Out = "\n0\n1\n2\n3\n4\n5\n6\n7\n8\n9\n"
        test = ErsMacroStream(test1In)
        test.__idx = 0
        test.__lineNo = 0
        test.__lineIdx = [0]
        testRes = ""
        c = test.__filteredChar()
        while c is not None:
            testRes += c
            c = test.__nextFilteredChar()
        if len(test.__lineIdx) is not 13:
            print("Test 1 : failed")
            print('Line index length should be 13 and is {}'.
                    format(len(test.__lineIdx)))
            return False
        if testRes != test1Out:
            print("Test 1 : failed")
            print('Error parsing """', end='')
            print(test1In, end='')
            print('"""')
            print('Obtained """', end='')
            print(testRes, end='')
            print('"""')
            print('Instead of """', end='')
            print(test1Out, end='')
            print('"""')
            return False
        print("Test 1 : success")

# Test multiline valid macro calls and invalid macro calls
# Should not filter out strings between macro name and (
        test2In = """
ERS_INFO("No error");
ERS_INFO  ("with spaces and comments" /* comment */);
/* ... */ ERS_INFO /* ...*/  ("with spaces and comments" /* comiment */); // ...
/* ... */ ERS_INFO /* ...*/  ("multiline" /* comment */) // ...
/* ... */ ; // <-- semi column here
ERS_ERROR("missing semi column")
ERS_ERROR("missing semi column") /* comments */ //...
ERS_ERROR("missing semi column") ///* comments */ //...
/**/ERS_ERROR /*!*/("missing semi column") ///* comments */ //...
/* ... */ ERS_ERROR /* ...*/  ("multiline" /* comment */ // ...
/* ... */ ) // <-- last parenthesis here and no semi column

/* ERS_INFO*/ERS_FATAL;
/* c /* c */ERS_FATALERS_
ERS_FATAL;"ERS_LOG\\"s"ERS_x
ERS_FATAL("missing trailing parenthesis";
"""
        test2Out = [
            (1, 'INFO', 1, 21, 1, 1),
            (1, 'INFO', 23, 75, 2, 2),
            (1, 'INFO', 87, 149, 3, 3),
            (1, 'INFO', 168, 232, 4, 5),
            (0, 'ERROR', 258, 290, 6, 6),
            (0, 'ERROR', 291, 323, 7, 7),
            (0, 'ERROR', 345, 377, 8, 8),
            (0, 'ERROR', 405, 443, 9, 9),
            (0, 'ERROR', 477, 542, 10, 11),
            (-1, 'FATAL', 605, 614, 13, 13),
            (-1, 'FATAL', 628, 637, 14, 14),
            (-1, 'FATAL', 642, 651, 15, 15),
            (-2, 'FATAL', 670, 712, 16, 17)]

        test = ErsMacroStream(test2In)
        testRes = test.getMacroTuples()

        #test.setBashColors()
        #print("-----------------------------------")
        #print('Error parsing """' + test2In + '"""')
        #for nm in testRes:
        #    print()
        #    print(nm)
        #    print('[' + test.__str[nm[2]:nm[3]] + ']')
        #    print(test.asString(nm,2))

        if testRes != test2Out:
            print("Test 2 : failed")
            print('Error parsing """' + test2In + '"""')
            print()
            print()
            print('Obtained')
            print(testRes)
            print()
            print('Instead of')
            print(test2Out)
            print()
            return False
        print("Test 2 : success")
        return True


def processFile(fileName, args):
    """Process specified file according to program argunents"""
    try:
        fileAsString = getFileAsString(fileName)
        parsed = ErsMacroStream(fileAsString)
        macroTuples = parsed.getMacroTuples()
        # set color
        if args.color:
            parsed.setBashColors()
        else:
            parsed.setColors(None)
    except:
        print("Processing", fileName)
        raise
    if not args.superQuiet or parsed.missing != 0 or parsed.invalid != 0:
        print("Processing", fileName, ":", end=' ')
        print(parsed.statusStr())
    if len(macroTuples) == 0 or (args.superQuiet and not args.apply):
        return

    out = tempfile.NamedTemporaryFile(delete=False) if args.apply else None
    lastIdx = 0
    try:
        # for each marcro found in the file
        for macroTuple in macroTuples:
            status = macroTuple[0]

            if status is 1:
                if args.valid and not args.quiet:
                    print(parsed.asString(macroTuple, args.lines), end='')
                    if args.interactive:
                        raw_input("Macro with ; Type <enter>...")
                    print()
            elif status is 0:
                if out is None:
                    if not args.quiet:
                        print(parsed.asString(macroTuple, args.lines), end='')
                        if args.interactive:
                            raw_input("Macro without ; type <enter>...")
                        print()
                else:
                    action = 'y'
                    if not args.quiet:
                        print(parsed.asString(macroTuple, args.lines), end='')
                        if args.interactive:
                            action = raw_input("Macro without ; Type 'y' to "
                                               "insert ; or <enter> to skip ")
                        print('')
                    if action == 'y':
                        out.write(parsed.substring(lastIdx, macroTuple[3]))
                        out.write(';')
                        lastIdx = macroTuple[3]
            else:
                if not args.quiet:
                    print(parsed.asString(macroTuple, args.lines), end='')
                    print("Invalid macro at line {} !".format(macroTuple[4]))
                    if args.interactive:
                        raw_input("Ignoring. type <enter>...")
                    print('')

        if lastIdx is not 0:
            out.write(parsed.substring(lastIdx, len(fileAsString)))
            tmpName = out.name
            out.close()
            out = None
            shutil.move(fileName, fileName + '.orig')
            shutil.move(tmpName, fileName)
    finally:
        if out is not None:
            tmpName = out.name
            out.close
            os.remove(tmpName)


if __name__ == "__main__":
    # parse options and arguments
    parser = argparse.ArgumentParser(prog=sys.argv[0],
        description="Fixing the ; after the ERS_XXX macros")
    parser.add_argument("-t", "--test", action="store_true",
                        default=False, help="test the FixERS.py script")
    parser.add_argument("-v", "--version",
                    action="store_true", default=False, help="show version")
    parser.add_argument("-r", "--recursive", action="store_true", default=False,
                        help="scan files in directories recursively")
    parser.add_argument("-d", "--debug", action="store_true", default=False,
                        help="display a stack trace in case of error")
    parser.add_argument("-a", "--apply", action="store_true", default=False,
                        help="apply the changes")
    parser.add_argument("-c", "--color", action="store_true",
                        default=False, help="add bash color to output")
    parser.add_argument("-V", "--valid", action="store_true", default=False,
                        help="display also macro calls with a trailing ;")
    parser.add_argument("-i", "--interactive", action="store_true",
        default=False, help="dispay found macro and ask action to perform")
    parser.add_argument("-l", "--lines", action="store", type=int, default=0,
                      help="number of contex lines to display with macro")
    parser.add_argument("-q", "--quiet", action="store_true", default=False,
                      help="don't display macro lines")
    parser.add_argument("-Q", "--superQuiet", action="store_true",
                        default=False,
                      help="display only status lines of files with macro "
                           "missing a trailing ;")
    parser.add_argument("file", help="one or more files to process", nargs='+')
    args = parser.parse_args()

    # Display version and quit
    if args.version:
        print(version)
        sys.exit(0)

    # Execute unite test
    try:
        if args.test:
            print("Testing the script...")
            succeeded = ErsMacroStream.unitTest()
            if not succeeded:
                print("Unit test failed !!")
                sys.exit(1)
            print("Test succeeded !!")
            sys.exit(0)
    except Exception as err:
        print(err)
        if args.debug:
            print(traceback.format_exc())
        sys.exit(1)

    # Process each file and directory in sequence
    if args.superQuiet:
        args.quiet = True
    failed = False
    for fileName in args.file:
        if os.path.isfile(fileName):
            processFile(fileName, args)
        elif os.path.isdir(fileName) and args.recursive:
            for root, dirs, files in os.walk(fileName):
                for dirName in skipDirs:
                    if dirName in dirs:
                        dirs.remove(dirName)
                for name in files:
                    if os.path.splitext(name)[1] in fileTypes:
                        try:
                            processFile(os.path.join(root, name), args)
                        except Exception as err:
                            failed = True
                            print("Processing aborted", err)
                            if args.debug:
                                print(traceback.format_exc())

    sys.exit(0 if not failed else 1)
