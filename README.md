# Fix ERS_XXX macros missing ;

## Introduction
The ERS_XXX macros like `ERS_ERROR`, `ERS_DEBUG` and `ERS_LOG` have not 
been defined with the classical `do { ... } while (0)` wrapper. As a 
consequence developpers have used the macro without appending a `;`
after its call. It is also unsure if the macro is a single instruction
or not. 

Fixing the macros by adding the `do { ... } while (0)` wrapper will
require that each macro call has a trailing `;` appended to it. 
Unfortunately, many TDAQ packages are using these macro and this
could break a lot of code in many places. 

The python script provided here (`FixERS.py`) is intended to
help users in this migration. It provides different options
in order to help evaluate the code status and eventually
apply the change. A list of the program options can be 
displayed by invoking the script with the option -h:

    FixERS.py -h


## Checking the status
Before making any changes, a developper may want to check the
status of its code regarding the ; after ERS_XXX macros:

    FixERS.py -c -q -r <files or directories>

- the option `-c` is for color output in bash shells ;
- the option `-q` is to display only a summary per file ;
- the option `-r` is to process directories recursively.

Check that all your source files have been listed. Files are filtered by
extentions. Only `.cxx`, `.cpp`, `.h`, `hpp`' `.hxx`, `.i` are considered.
If a file type is missing you may add it to the the variable `fileTypes`
at the start of the script code.

When processing directories recursively, some special directories will be 
skiped because they may confuse FixERS.py. These are the directories 
`.git`, `.svn` and `.hg`. If other directories need to be ignored, add 
them in the variable `skipDirs` at the top of the python script.

With the `-Q` (superQuiet) option, only the files containing one or more
macro with a missing ; or invalid macro calls are listed. Without the `-q` 
options, all lines containing a macro without a trailing ;
are displayed. With the `-c` option the missing ; is added in red where it
will be inserted when requested.

If you want the program to pause after displaying each macro, use the option 
`-i`. You will have to type \<enter> to display the next macro found in the
file. 

To display some lines before and after the lines containing the macro, use 
the option `-l` and specify the number of lines to add before and after. 
`-l 2` is a good choice when using the interactive mode.

By default FixERS.py only shows macros without a trailing ;. The option
`-V` (capital V) will result in displaying also the with a trailing ;.
If you added the `-c` option for color output, the ; will be displayed
in green.       

## Applying the changes
Once you have checked that the proposed ; is valid. You can apply the 
change with the option `-a`. When `-a` is combined with the option `-i`
for interactive mode, you will be asked for each macro, one by one, if
the change must be applied.

If a file has to be modified, the original file is first copied in a 
file with the same name and `.orig` appended to it as backup. The 
changes are then saved in the file.

It is possible to check the changes by comparing the file with its 
original version and revert to the original file by renaming the backup
file to its original name.

## Known bugs
Colors are not working when piping the output of FixERS.py in `less`
or equivalent tools.
